[![Codacy Badge](https://app.codacy.com/project/badge/Grade/42fe18ead70d4213bbab9eea0945704e)](https://www.codacy.com/gl/sven.kroell/bank-account-toy/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=sven.kroell/bank-account-toy&amp;utm_campaign=Badge_Grade)

# Bank Account Toy
A Rest api for getting account data and do some transfers

## Usage
### Start Local
You can start it locally with `./gradlew bootRun`

### Start Container
To use the prebuild contaienr just use docker-compose up

### Documentation
You can find a swagger documentation at:  http://localhost:8080/swagger-ui.html

## Missing things
These are things which are missing where I didn't have the time or knowledge by heart to effectively add the functionality
I decided to rather build a more wholesome approach.

### Security
Adding a proper Security endpoint with JWT would have taken more time than I had. 

Right now I would not call it production-ready on a security level

### Testing
Even though I am very invested in Testing - I did only add a few unit tests which again I would not call production ready.

To make it more production ready it would again take more time to properly write some more unit/component tests as well as RestAssured Tests for Integration

### Database
Right now I use an in memory h2 database. Adding a PostgreSQL and implementing adding it to the deployment stategy would make it production ready

### Deployment
I don't like deploying an application with docker compose in a production environment

## Logging/Observability
In order to call this service Production Ready it still needs proper logging

### Proper response objects
Right now I am sending DTOs instead of Response Bodies

### Missing Code Coverage
Right now there's no implementation of Jacoco installed it would have taken too much time to add this

## Sonarqube
I did not have the time to add sonarqube, but I used codacy as this was just a click.

### Documentation on class and function level
This was not added and I don't see it as purely necessary therefore I disabled the checks in Codacy

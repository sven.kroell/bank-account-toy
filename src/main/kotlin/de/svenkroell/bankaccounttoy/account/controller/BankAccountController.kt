package de.svenkroell.bankaccounttoy.account.controller

import de.svenkroell.bankaccounttoy.account.dto.AmountDto
import de.svenkroell.bankaccounttoy.account.dto.BankAccountsDto
import de.svenkroell.bankaccounttoy.account.dto.TransactionsDto
import de.svenkroell.bankaccounttoy.account.services.BankAccountService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/accounts")
class BankAccountController(private val bankAccountService: BankAccountService) {
    @GetMapping("/")
    fun getAllAccounts(): ResponseEntity<BankAccountsDto> {
        return ResponseEntity.ok(bankAccountService.getAllAccounts())
    }

    @GetMapping("/{iban}/amount")
    fun getAmountOfAccount(@PathVariable iban: String): ResponseEntity<AmountDto> {
        return ResponseEntity.ok(bankAccountService.getAmountForAccount(iban))
    }

    @GetMapping("/types/{type}")
    fun getAccountByType(@PathVariable type: String): ResponseEntity<BankAccountsDto> {
        return ResponseEntity.ok(bankAccountService.getAccountsByType(type))
    }

    @GetMapping("/{iban}/transactions")
    fun getTransactionsForAccount(@PathVariable iban: String): ResponseEntity<TransactionsDto> {
        return ResponseEntity.ok(bankAccountService.getTransactionsForAccount(iban))
    }
}

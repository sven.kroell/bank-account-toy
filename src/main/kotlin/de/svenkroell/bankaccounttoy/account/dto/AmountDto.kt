package de.svenkroell.bankaccounttoy.account.dto

data class AmountDto(val iban: String, val amount: String)

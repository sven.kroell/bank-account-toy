package de.svenkroell.bankaccounttoy.account.dto

import de.svenkroell.bankaccounttoy.account.models.BankAccount

data class BankAccountsDto(val listOfBankAccounts: List<BankAccount>)

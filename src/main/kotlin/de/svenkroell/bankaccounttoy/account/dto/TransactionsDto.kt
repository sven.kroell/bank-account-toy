package de.svenkroell.bankaccounttoy.account.dto

import de.svenkroell.bankaccounttoy.transaction.models.Transaction

data class TransactionsDto(val iban: String, val transactons: List<Transaction>)

package de.svenkroell.bankaccounttoy.account.exceptions

class AccountNotFoundException : RuntimeException()

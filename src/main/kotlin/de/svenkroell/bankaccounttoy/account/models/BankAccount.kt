package de.svenkroell.bankaccounttoy.account.models

import de.svenkroell.bankaccounttoy.transaction.models.Transaction
import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class BankAccount(
        @Id
        val iban: String,
        val amount: BigDecimal,
        @OneToMany
        val transactions: List<Transaction>,
        val typeOfAccount: TypesOfAccounts,
        val referenceAccount: String = ""
)

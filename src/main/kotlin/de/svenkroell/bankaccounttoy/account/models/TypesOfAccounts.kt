package de.svenkroell.bankaccounttoy.account.models

enum class TypesOfAccounts {
    CHECKING, PRIVATE_LOAN, SAVING
}

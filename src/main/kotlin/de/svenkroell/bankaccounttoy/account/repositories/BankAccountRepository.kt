package de.svenkroell.bankaccounttoy.account.repositories

import de.svenkroell.bankaccounttoy.account.models.BankAccount
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface BankAccountRepository : CrudRepository<BankAccount, String>

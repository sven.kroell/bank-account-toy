package de.svenkroell.bankaccounttoy.account.services

import de.svenkroell.bankaccounttoy.account.dto.AmountDto
import de.svenkroell.bankaccounttoy.account.dto.BankAccountsDto
import de.svenkroell.bankaccounttoy.account.dto.TransactionsDto
import de.svenkroell.bankaccounttoy.account.exceptions.AccountNotFoundException
import de.svenkroell.bankaccounttoy.account.models.BankAccount
import de.svenkroell.bankaccounttoy.account.repositories.BankAccountRepository
import org.springframework.stereotype.Service

@Service
class BankAccountService(private val bankAccountRepository: BankAccountRepository) {
    fun getAllAccounts(): BankAccountsDto {
        val accounts = bankAccountRepository.findAll()
        return BankAccountsDto(accounts.toList())
    }

    fun getAmountForAccount(iban: String): AmountDto {
        val account = getAccountByIban(iban)
        return AmountDto(account.iban, account.amount.toString())
    }

    fun getAccountsByType(type: String): BankAccountsDto {
        val accounts = bankAccountRepository.findAll()
        val accountsByType = accounts.filter { account -> account.typeOfAccount.name == type.toUpperCase() }
        return BankAccountsDto(accountsByType)
    }

    fun getAccountByIban(iban: String): BankAccount {
        return bankAccountRepository.findById(iban).orElseThrow { AccountNotFoundException() }
    }

    fun getTransactionsForAccount(iban: String): TransactionsDto {
        val account = getAccountByIban(iban)
        return TransactionsDto(account.iban, account.transactions)
    }

    fun updateAccount(updatedAccount: BankAccount): BankAccount {
        return bankAccountRepository.save(updatedAccount)
    }
}

package de.svenkroell.bankaccounttoy.shared

import de.svenkroell.bankaccounttoy.shared.services.PopulationService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class DatabaseInitialisation(private val populationService: PopulationService) {
    val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    @PostConstruct
    fun populateDb() {
        populationService.populateDB()
        logger.info("Database Populated")
    }
}

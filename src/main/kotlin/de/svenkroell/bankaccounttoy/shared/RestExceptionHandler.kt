package de.svenkroell.bankaccounttoy.shared

import de.svenkroell.bankaccounttoy.account.exceptions.AccountNotFoundException
import de.svenkroell.bankaccounttoy.transaction.exceptions.AmountNegativeException
import de.svenkroell.bankaccounttoy.transaction.exceptions.OperationNotAllowedException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
class RestExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(AccountNotFoundException::class)
    fun handleAccountNotFoundException(accountNotFoundException: AccountNotFoundException): ResponseEntity<String> {
        // TODO: Add proper error logging
        logger.info(accountNotFoundException.message)
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The Account does not exist")
    }

    @ExceptionHandler(OperationNotAllowedException::class)
    fun handleOperationNotAllowedException(operationNotAllowedException: OperationNotAllowedException): ResponseEntity<String> {
        logger.info(operationNotAllowedException.message)
        return ResponseEntity.status(400).body("Problem with performing the transfer")
    }

    @ExceptionHandler(AmountNegativeException::class)
    fun handleAmountNegativeException(amountNegativeException: AmountNegativeException): ResponseEntity<String> {
        logger.info("Deposit of negative amount has been performed")
        return ResponseEntity.badRequest().body("Negative amount not allowed")
    }
}

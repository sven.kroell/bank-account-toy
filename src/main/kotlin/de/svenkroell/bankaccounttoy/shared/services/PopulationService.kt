package de.svenkroell.bankaccounttoy.shared.services

import de.svenkroell.bankaccounttoy.account.models.BankAccount
import de.svenkroell.bankaccounttoy.account.models.TypesOfAccounts
import de.svenkroell.bankaccounttoy.account.repositories.BankAccountRepository
import org.springframework.stereotype.Service

@Service
class PopulationService(private val accountRepository: BankAccountRepository) {
    fun populateDB() {
        val checkingAccount = BankAccount("DE75512108001245126199", 5000.toBigDecimal(), listOf(), TypesOfAccounts.CHECKING)
        val checkingAccount2 = BankAccount("DE34500105176852982653", "1000.50".toBigDecimal(), listOf(), TypesOfAccounts.CHECKING)
        val checkingAccount3 = BankAccount("DE41500105178715948134", 0.toBigDecimal(), listOf(), TypesOfAccounts.CHECKING)
        val savingAccount = BankAccount("DE96500105171751766281", 20000.toBigDecimal(), listOf(), TypesOfAccounts.SAVING, "DE75512108001245126199")
        val savingAccount2 = BankAccount("DE83500105171369479427", 10.toBigDecimal(), listOf(), TypesOfAccounts.SAVING, "DE75512108001245126199")
        val privateLoanAccount = BankAccount("DE02500105174612121239", 30000.toBigDecimal(), listOf(), TypesOfAccounts.PRIVATE_LOAN)

        accountRepository.save(checkingAccount)
        accountRepository.save(checkingAccount2)
        accountRepository.save(checkingAccount3)
        accountRepository.save(savingAccount)
        accountRepository.save(savingAccount2)
        accountRepository.save(privateLoanAccount)
    }
}

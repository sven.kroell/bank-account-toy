package de.svenkroell.bankaccounttoy.transaction.controller

import de.svenkroell.bankaccounttoy.transaction.dto.DepositDto
import de.svenkroell.bankaccounttoy.transaction.dto.TransferDto
import de.svenkroell.bankaccounttoy.transaction.requestbodies.DepositRequestBody
import de.svenkroell.bankaccounttoy.transaction.requestbodies.TransferRequestBody
import de.svenkroell.bankaccounttoy.transaction.services.TransactionService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/transaction")
class TransactionsController(private val transactionService: TransactionService) {

    @PostMapping("/deposit")
    fun depositMoney(@RequestBody depositRequestBody: DepositRequestBody): ResponseEntity<DepositDto> {
        return ResponseEntity.ok(transactionService.depositMoney(depositRequestBody.iban, depositRequestBody.amount))
    }

    @PostMapping("/transfer")
    fun transferMoney(@RequestBody transferRequestBody: TransferRequestBody): ResponseEntity<TransferDto> {
        return ResponseEntity.ok(transactionService.transferMoney(transferRequestBody.sourceIban, transferRequestBody.targetIban, transferRequestBody.amount))
    }
}

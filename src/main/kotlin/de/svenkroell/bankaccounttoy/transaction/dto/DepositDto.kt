package de.svenkroell.bankaccounttoy.transaction.dto

data class DepositDto(val status: String, val transactionData: TransactionData)

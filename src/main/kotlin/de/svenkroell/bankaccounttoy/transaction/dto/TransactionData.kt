package de.svenkroell.bankaccounttoy.transaction.dto

data class TransactionData(val iban: String, val amount: String)

package de.svenkroell.bankaccounttoy.transaction.dto

data class TransferDto(
        val status: String,
        val sourceTransactionData: TransactionData,
        val targetTransactionData: TransactionData)

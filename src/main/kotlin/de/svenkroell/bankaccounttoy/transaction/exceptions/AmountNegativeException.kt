package de.svenkroell.bankaccounttoy.transaction.exceptions

class AmountNegativeException : RuntimeException()

package de.svenkroell.bankaccounttoy.transaction.exceptions

class OperationNotAllowedException(message: String) : RuntimeException(message)

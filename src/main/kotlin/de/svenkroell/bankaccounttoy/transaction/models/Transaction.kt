package de.svenkroell.bankaccounttoy.transaction.models

import java.math.BigDecimal
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Transaction(
        @Id
        val id: UUID = UUID.randomUUID(),
        val typeOfTransactions: TypesOfTransactions,
        val target: String,
        val source: String,
        val amount: BigDecimal
)


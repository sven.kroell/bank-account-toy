package de.svenkroell.bankaccounttoy.transaction.models

enum class TypesOfTransactions {
    Deposit, Transfer
}

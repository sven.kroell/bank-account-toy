package de.svenkroell.bankaccounttoy.transaction.repositories

import de.svenkroell.bankaccounttoy.transaction.models.Transaction
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface TransactionRepository : CrudRepository<Transaction, UUID>

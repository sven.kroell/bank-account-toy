package de.svenkroell.bankaccounttoy.transaction.requestbodies

data class DepositRequestBody(val iban: String, val amount: String)

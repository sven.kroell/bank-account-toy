package de.svenkroell.bankaccounttoy.transaction.requestbodies

data class TransferRequestBody(val sourceIban: String, val targetIban: String, val amount: String)

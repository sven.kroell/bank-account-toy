package de.svenkroell.bankaccounttoy.transaction.services

import de.svenkroell.bankaccounttoy.account.models.BankAccount
import de.svenkroell.bankaccounttoy.account.services.BankAccountService
import de.svenkroell.bankaccounttoy.transaction.dto.DepositDto
import de.svenkroell.bankaccounttoy.transaction.dto.TransactionData
import de.svenkroell.bankaccounttoy.transaction.dto.TransferDto
import de.svenkroell.bankaccounttoy.transaction.models.Transaction
import de.svenkroell.bankaccounttoy.transaction.models.TypesOfTransactions
import de.svenkroell.bankaccounttoy.transaction.repositories.TransactionRepository
import de.svenkroell.bankaccounttoy.transaction.validation.TransactionValidations.checkAmountBelowZero
import de.svenkroell.bankaccounttoy.transaction.validation.TransactionValidations.checkIfPrivateLoan
import de.svenkroell.bankaccounttoy.transaction.validation.TransactionValidations.checkIfSameAccount
import de.svenkroell.bankaccounttoy.transaction.validation.TransactionValidations.checkSavingAccountTransfer
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal

@Service
class TransactionService(private val accountService: BankAccountService,
                         private val transactionRepository: TransactionRepository) {

    @Transactional
    fun depositMoney(iban: String, amount: String): DepositDto {
        val decimalAmount = amount.toBigDecimal()
        checkAmountBelowZero(decimalAmount)
        val account = accountService.getAccountByIban(iban)
        val updatedAccount = performTransaction(
                typeOfTransaction = TypesOfTransactions.Deposit,
                source = "deposit",
                target = account.iban,
                amount = decimalAmount,
                account = account
        )
        return DepositDto("success", TransactionData(updatedAccount.iban, updatedAccount.amount.toString()))
    }

    @Transactional
    fun transferMoney(sourceIban: String, targetIban: String, amount: String): TransferDto {
        val decimalAmount = amount.toBigDecimal()
        checkIfSameAccount(sourceIban, targetIban)
        checkAmountBelowZero(decimalAmount)
        val sourceAccount = accountService.getAccountByIban(sourceIban)
        checkSavingAccountTransfer(sourceAccount, targetIban)
        checkIfPrivateLoan(sourceAccount)
        val targetAccount = accountService.getAccountByIban(targetIban)
        val updatedSourceAccount = performTransaction(
                typeOfTransaction = TypesOfTransactions.Transfer,
                target = targetAccount.iban,
                source = sourceAccount.iban,
                amount = decimalAmount.negate(),
                account = sourceAccount
        )
        val updatedDestinationAccount = performTransaction(
                typeOfTransaction = TypesOfTransactions.Transfer,
                target = targetAccount.iban,
                source = sourceAccount.iban,
                amount = decimalAmount,
                account = targetAccount
        )
        return TransferDto(status = "ok",
                TransactionData(updatedSourceAccount.iban, updatedSourceAccount.amount.toString()),
                TransactionData(updatedDestinationAccount.iban, updatedDestinationAccount.amount.toString()))
    }

    private fun performTransaction(typeOfTransaction: TypesOfTransactions,
                                   target: String,
                                   source: String,
                                   amount: BigDecimal,
                                   account: BankAccount): BankAccount {
        val transaction = Transaction(
                typeOfTransactions = typeOfTransaction,
                target = target,
                source = source,
                amount = amount
        )
        val updatedAccount = account.copy(
                amount = account.amount.add(amount),
                transactions = listOf(*account.transactions.toTypedArray(), transaction)
        )
        transactionRepository.save(transaction)
        return accountService.updateAccount(updatedAccount)
    }
}

package de.svenkroell.bankaccounttoy.transaction.validation

import de.svenkroell.bankaccounttoy.account.models.BankAccount
import de.svenkroell.bankaccounttoy.account.models.TypesOfAccounts
import de.svenkroell.bankaccounttoy.transaction.exceptions.AmountNegativeException
import de.svenkroell.bankaccounttoy.transaction.exceptions.OperationNotAllowedException
import java.math.BigDecimal

object TransactionValidations {
    fun checkIfSameAccount(sourceIban: String, targetIban: String) {
        if (sourceIban == targetIban) {
            throw OperationNotAllowedException("Transfer to the same account is not allowed")
        }
    }

    fun checkAmountBelowZero(decimalAmount: BigDecimal) {
        if (decimalAmount < BigDecimal.ZERO) {
            throw AmountNegativeException()
        }
    }

    fun checkSavingAccountTransfer(sourceAccount: BankAccount, targetIban: String) {
        if (sourceAccount.typeOfAccount == TypesOfAccounts.SAVING) {
            if (sourceAccount.referenceAccount != targetIban) {
                throw OperationNotAllowedException("Transfer is only allowed to the reference account")
            }
        }
    }

    fun checkIfPrivateLoan(sourceAccount: BankAccount) {
        if (sourceAccount.typeOfAccount == TypesOfAccounts.PRIVATE_LOAN) {
            throw OperationNotAllowedException("Deducting money from Loan account not allowed")
        }
    }
}

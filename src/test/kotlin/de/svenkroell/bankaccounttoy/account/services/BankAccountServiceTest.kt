package de.svenkroell.bankaccounttoy.account.services

import de.svenkroell.bankaccounttoy.account.exceptions.AccountNotFoundException
import de.svenkroell.bankaccounttoy.account.models.BankAccount
import de.svenkroell.bankaccounttoy.account.models.TypesOfAccounts
import de.svenkroell.bankaccounttoy.account.repositories.BankAccountRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import java.util.*

internal class BankAccountServiceTest {
    @Test
    fun `two saving accounts exist - get accounts by type saving - returns both`() {
        val mockRepository = Mockito.mock(BankAccountRepository::class.java)
        val bankAccountSaving1 = BankAccount("1234", "123.40".toBigDecimal(), listOf(), TypesOfAccounts.SAVING, "")
        val bankAccountSaving2 = BankAccount("1234", "123.40".toBigDecimal(), listOf(), TypesOfAccounts.SAVING, "")
        val bankAccountChecking = BankAccount("1234", "123.40".toBigDecimal(), listOf(), TypesOfAccounts.CHECKING)
        Mockito.`when`(mockRepository.findAll()).thenReturn(mutableListOf(bankAccountSaving1, bankAccountSaving2, bankAccountChecking))

        val bankAccountService = BankAccountService(mockRepository)

        val accounts = bankAccountService.getAccountsByType("saving")

        assertEquals(accounts.listOfBankAccounts.size, 2)
    }

    @Test
    fun `account not in database - get amount for account - throws exception`() {
        val mockRepository = Mockito.mock(BankAccountRepository::class.java)
        Mockito.`when`(mockRepository.findById("1234")).thenReturn(Optional.empty())
        val bankAccountService = BankAccountService(mockRepository)

        assertThrows<AccountNotFoundException> { bankAccountService.getAmountForAccount("1234") }
    }
}
